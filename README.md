#

Airflow DAG for annotating the Labs REST API journals route.

# Install for development

- `python -m venv venv`
- `. /venv/bin/activate`
- `pip install --upgrade pip`
- `pip install -e .`
- `pip install .[dev]`
