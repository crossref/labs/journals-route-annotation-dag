## Notes

## More than just journal ISSNs

There are more than just journals ISSNs.

For example:

Conference-series have ISSNs:

<http://api.crossref.org/works?filter=type:proceedings-series>

## On Crossref metdata and API

Book series have ISSNs:

<http://api.crossref.org/works?filter=type:book-series>

Report series have ISSNs:

<http://api.crossref.org/works?filter=type:report-series>

 And although these are listed in works, then do not seem to be listed in the titleList and are not available via the `/journals` route.

This means the `/journals` route is an incomplete list of ISSNs.

We should probably have the following routes at least:

- `/series`
- `/journals`
- `/proceedings-series/`
- `/book-series/`
- `/report-series/`

This also has implications for us having a `container-id`. A book in a book-series, for example, could have an ISSN *and* an ISBN. Which is the "container-id"?

## On Airflow in AWS

- TIL: You cannot pass generators between tasks
