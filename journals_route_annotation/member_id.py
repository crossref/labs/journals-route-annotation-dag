import json
import logging
import os

from joblib import Parallel, delayed

from journals_route_annotation.constants import (
    ANNOTATIONS_WORKING_PATH,
    USER_AGENT,
    WORK_SAMPLE_URL_TEMPLATE,
    MEMBER_ID_KEY,
)

# from journals_route_annotation.container_ids import has_issn, issns
from journals_route_annotation.crutils import (
    construct_s3_key,
    batched,
    load_from_s3,
    save_to_s3,
    tenacious_get,
)

logging.basicConfig(level=os.environ.get("LOG_LEVEL", logging.WARNING))
logger = logging.getLogger(__name__)


def get_member_id(issn):
    """Return the member id for a given issn by looking up a sample work for the issn"""
    url = WORK_SAMPLE_URL_TEMPLATE.format(issn=issn)
    logger.debug(f"Fetching {url} with {USER_AGENT}")
    sample = tenacious_get(url, headers=USER_AGENT)
    if not sample:
        logger.warning(f"Could fetch member data for: {issn}")
        return None

    sample = json.loads(sample)
    if sample["message"]["items"]:
        if member_id := sample["message"]["items"][0].get("member"):
            logger.debug(f"Member ID for {issn}: {member_id}")
            return int(member_id)

    logger.warning(f"Could not get member ID for: {issn}")
    return None


def get_issn_owner_ids(
    title_list_issn_mapping_fn, max_workers=1, debug_max=None, run_prefix=None
):
    entries = json.loads(load_from_s3(title_list_issn_mapping_fn))
    # if debug_max is defined, take on the first debug_max entries from the entries dictionary
    if debug_max:
        entries = dict(list(entries.items())[:debug_max])

    for batch in batched(entries, max_workers):
        results = Parallel(n_jobs=max_workers, prefer="threads")(
            delayed(get_member_id)(issn) for issn in batch
        )
        for issn, member_id in zip(batch, results):
            entries[issn][MEMBER_ID_KEY] = member_id
    # save to s3
    key = construct_s3_key(
        ANNOTATIONS_WORKING_PATH, f"{run_prefix}", "title_list_issn_owner_ids.json"
    )
    save_to_s3(key, json.dumps(entries, indent=2), "application/json")
    return key
