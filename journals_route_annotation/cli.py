import json
import logging
import os
from datetime import datetime, timezone

import typer

from journals_route_annotation.annotator import parallel_add_annotations
from journals_route_annotation.crutils import cleanup_working_path
from journals_route_annotation.member_id import get_issn_owner_ids
from journals_route_annotation.title_list import get_title_list, map_title_list_to_issns

logging.basicConfig(level=os.environ.get("LOG_LEVEL", logging.WARNING))
logger = logging.getLogger(__name__)


def cli(
    max_workers: int = typer.Option(20, help="max workers"),
    debug_max: int = typer.Option(None, help="debug max"),
    verbose: bool = typer.Option(False, help="verbose"),
):
    if verbose:
        logger.setLevel(logging.DEBUG)
        logger.info("Verbose logging enabled")

    logger.info("Starting journals_route_annotation")

    run_prefix = f"cli-{datetime.now(timezone.utc).isoformat()}"

    logger.info("Getting title list")
    title_list_fn = get_title_list(run_prefix=run_prefix)
    logger.info("Mapping title list to ISSNs")
    title_list_issn_mapping_fn = map_title_list_to_issns(
        title_list_fn, run_prefix=run_prefix
    )
    logger.info("Getting ISSN owner IDs")
    issn_owner_ids_fn = get_issn_owner_ids(
        title_list_issn_mapping_fn,
        max_workers=max_workers,
        debug_max=debug_max,
        run_prefix=run_prefix,
    )
    logger.info("Annotating")
    results = parallel_add_annotations(issn_owner_ids_fn, max_workers=max_workers)
    cleanup_working_path(run_prefix=run_prefix)
    logger.info("Done")
    print(json.dumps(results, indent=2))


def main():
    typer.run(cli)


if __name__ == "__main__":
    main()
