# # get the name of this module
module_name = __name__.split(".")[0]

MAX_WORKERS = 1000
# DEBUG_MAX = None

# API_BASE = "https://api.crossref.org"
USER_AGENT = {"User-Agent": f"Crossref Labs ({module_name}); mailto:labs@crossref.org"}


TITLE_LIST_URL = "http://ftp.crossref.org/titlelist/titleFile.csv"


WORK_SAMPLE_URL_TEMPLATE = "https://api.crossref.org/works?filter=issn:{issn}"


ANNOTATIONS_BUCKET = "outputs.research.crossref.org"
ANNOTATIONS_PATH = "annotations"
ANNOTATIONS_WORKING_PATH = "annotations-working"


CR_CONTAINER_ID_KEY = "crcid"  # key for crossref container id within an annotation
CR_CONTAINER_ID_URI_KEY = (
    f"{CR_CONTAINER_ID_KEY}-uri"  # key for crossref container id expressed as a URI
)

PUBID_PREFIX = "https://id.crossref.org/crcid/"  # prefix for crossref container id expressed as a URI

CONTAINER_IDS_KEY = (
    "container-ids"  # key for container id annotations within an API result
)

MEMBER_ID_KEY = "member-id"  # key for member id within an annotation


ISSN_KEYS = ["pissn", "eissn"]  # keys for issn within an annotation

# CONTAINER_KEYS = ISSN_KEYS + [
#     "doi",
#     CR_CONTAINER_ID_KEY,
#     f"{CR_CONTAINER_ID_KEY}-uri",
# ]  # keys for container id annotations within an API result


CONTAINER_KEYS = [
    "doi",
    CR_CONTAINER_ID_KEY,
    f"{CR_CONTAINER_ID_KEY}-uri",
]  # keys for container id annotations within an API result


# ABOUT_MEMBER_ID = "https://www.crossref.org/labs/labs-rest-api#about-member-id"
# ABOUT_CONTAINER_ID = "https://www.crossref.org/labs/labs-rest-api#about-container-id"
