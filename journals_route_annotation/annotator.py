import json
import logging
import os
from datetime import datetime, timezone
from joblib import Parallel, delayed

from journals_route_annotation.constants import (
    ANNOTATIONS_PATH,
    MAX_WORKERS,
    CONTAINER_KEYS,
    CONTAINER_IDS_KEY,
    MEMBER_ID_KEY,
)
from journals_route_annotation.crutils import (
    batched,
    load_from_s3,
    save_to_s3,
    construct_s3_key,
)

logging.basicConfig(level=os.environ.get("LOG_LEVEL", logging.INFO))
logger = logging.getLogger(__name__)


def subdict(d, keys):
    return {key: d[key] for key in keys if key in d}


def about(key):
    return {
        "about": {
            "about-this-metadata": f"https://www.crossref.org/labs/labs-rest-api#about-{key}",
            "metadata-last-updated": datetime.now(timezone.utc).isoformat(),
        }
    }


def annotate_container_ids(issn, entry):
    annotation = subdict(entry, CONTAINER_KEYS) | about("container-id")
    key = construct_s3_key(
        ANNOTATIONS_PATH, "journals", f"{issn}/{CONTAINER_IDS_KEY}.json"
    )
    save_to_s3(key, json.dumps(annotation, indent=2), "application/json")


def annotate_owner_ids(issn, entry):
    annotation = subdict(entry, [MEMBER_ID_KEY]) | about(MEMBER_ID_KEY)
    key = construct_s3_key(ANNOTATIONS_PATH, "journals", f"{issn}/{MEMBER_ID_KEY}.json")
    save_to_s3(key, json.dumps(annotation, indent=2), "application/json")


def annotate(issn, entry):
    try:
        logger.debug(f"annotating: {issn}")
        # save to s3
        annotate_container_ids(issn, entry)
        annotate_owner_ids(issn, entry)

    except Exception as err:
        logger.warning(f"error: {err}")
    return entry


def parallel_add_annotations(title_list_issn_mapping_fn, max_workers=MAX_WORKERS):
    results = []
    logger.info(f"annotating: {max_workers}")
    entries = json.loads(load_from_s3(title_list_issn_mapping_fn))
    for batch in batched(entries, max_workers):
        results.extend(
            Parallel(n_jobs=max_workers, prefer="threads")(
                delayed(annotate)(issn, entries[issn]) for issn in batch
            )
        )

    return results
