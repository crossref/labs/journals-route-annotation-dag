import csv
import json
import logging
import os


from journals_route_annotation.constants import (
    ANNOTATIONS_WORKING_PATH,
    CR_CONTAINER_ID_KEY,
    CR_CONTAINER_ID_URI_KEY,
    PUBID_PREFIX,
    TITLE_LIST_URL,
    USER_AGENT,
)
from journals_route_annotation.crutils import (
    construct_s3_key,
    load_from_s3,
    save_to_s3,
    tenacious_get,
    normalize_id,
)

logging.basicConfig(level=os.environ.get("LOG_LEVEL", logging.WARNING))
logger = logging.getLogger(__name__)


def text2csv(text):
    """Convert text to CSV"""
    logger.debug("Converting text to CSV")

    lines = text.splitlines()
    return csv.DictReader(lines)


def csv2dicts(csv_data):
    """Convert CSV to an array of dicts"""
    logger.debug("Converting CSV to JSON")
    return [dict(row) for row in csv_data]


def load_title_list_to_dict(title_list_key):
    """Load title list to dict"""
    title_list_text = load_from_s3(title_list_key)
    return csv2dicts(text2csv(title_list_text))


def issn_types_present(entry):
    """Return a list of issn types present"""
    issn_types = [issn_type for issn_type in ("pissn", "eissn") if entry.get(issn_type)]
    if not issn_types:
        logger.warning(f"No ISSNs found for entry {entry.get('JournalID')}")
    return issn_types


def map_title_list_to_issns(title_list_key, run_prefix=None):
    """Map title list to issn"""

    entries = load_title_list_to_dict(title_list_key)

    issn_map = {
        normalize_id(entry[issn_type]): {
            CR_CONTAINER_ID_KEY: entry.get("JournalID", ""),
            CR_CONTAINER_ID_URI_KEY: PUBID_PREFIX + entry.get("JournalID", ""),
            "doi": entry.get("doi", ""),
        }
        for entry in entries
        for issn_type in issn_types_present(entry)
    }

    # Construct S3 key
    s3_key = construct_s3_key(
        ANNOTATIONS_WORKING_PATH, run_prefix, "title_list_issn_mapping.json"
    )
    save_to_s3(s3_key, json.dumps(issn_map, indent=2), "application/json")
    return s3_key


def get_title_list(run_prefix=None):
    """Get the title list from Crossref, be stubborn about it"""
    logger.debug(f"Fetching title list from {TITLE_LIST_URL} with {USER_AGENT}")
    title_list = tenacious_get(TITLE_LIST_URL, headers=USER_AGENT)
    logger.debug(f"Fetched title list. Length: {len(title_list)}")
    # save to s3
    # key = os.path.join(ANNOTATIONS_WORKING_PATH, f"{run_prefix}", "title_list.csv")
    key = construct_s3_key(ANNOTATIONS_WORKING_PATH, run_prefix, "title_list.csv")
    save_to_s3(key, title_list, "text/csv")

    return key
