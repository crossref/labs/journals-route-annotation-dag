import logging
import os
from itertools import islice

import boto3
import httpx
from tenacity import retry, wait_random_exponential

from journals_route_annotation.constants import (
    ANNOTATIONS_BUCKET,
    ANNOTATIONS_WORKING_PATH,
)

logging.basicConfig(level=os.environ.get("LOG_LEVEL", logging.WARNING))
logger = logging.getLogger(__name__)


@retry(wait=wait_random_exponential(multiplier=1, max=5))
def tenacious_get(url, headers):
    """Get a URL, retrying on failure"""
    try:
        with httpx.Client(headers=headers) as client:
            response = client.get(url, timeout=60.0)
            response.raise_for_status()  # Raise an exception for HTTP error responses
            return response.text
    except httpx.HTTPStatusError as e:
        logger.error(f"HTTP error occurred: {e.response.status_code}")
        raise
    except httpx.RequestError as e:
        logger.error(f"Request error occurred: {e}")
        raise


def batched(iterable, n):
    """Batch an iterable into chunks of size n"""
    # batched('ABCDEFG', 3) --> ABC DEF G
    if n < 1:
        raise ValueError("n must be at least one")
    it = iter(iterable)
    while batch := tuple(islice(it, n)):
        yield batch


def normalize_id(identifier):
    """Normalize ISSN"""
    return identifier.replace("-", "").upper() if identifier else None


def construct_s3_key(base_path, prefix, filename):
    """Helper function to construct S3 key"""
    return os.path.join(base_path, prefix or "", filename)


def save_to_s3(key, content, content_type):
    """Save content to S3"""
    logger.debug("Saving content to S3")
    s3 = boto3.resource("s3")
    bucket = s3.Bucket(ANNOTATIONS_BUCKET)
    bucket.put_object(
        Key=key,
        Body=content,
        ContentType=content_type,
    )


# load from s3
def load_from_s3(key):
    """Load content from S3"""
    logger.debug("Loading title list from S3")
    s3 = boto3.resource("s3")
    bucket = s3.Bucket(ANNOTATIONS_BUCKET)
    return bucket.Object(key).get()["Body"].read().decode("utf-8")


def cleanup_working_path(run_prefix):
    """Delete dag run working subdirectory S3"""
    path = f"{ANNOTATIONS_WORKING_PATH}/{run_prefix}"
    logger.debug(f"Deleting dag run working subdirectory S3: {path}")
    # s3 = boto3.resource("s3")
    # bucket = s3.Bucket(ANNOTATIONS_BUCKET)
    # bucket.objects.filter(Prefix=path).delete()
