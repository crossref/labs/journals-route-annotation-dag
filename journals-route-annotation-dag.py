import os
import hashlib
import pendulum
from airflow.decorators import dag, task


script_name = os.path.basename(__file__).replace(".py", "").replace("_", "-")

DAG_ID = script_name

DEFAULT_ARGS = {
    "owner": "gbilder",
    "depends_on_past": False,
    "email": ["labs@crossref.org"],
    "email_on_failure": False,
    "email_on_retry": False,
}


LIB_REQUIREMENTS = [
    "git+https://gitlab.com/crossref/labs/journals-route-annotation-dag.git",
    # "file:///Users/gbilder/src/journals-route-annotation-dag",
]


@dag(
    dag_id=f"{DAG_ID}",
    default_args=DEFAULT_ARGS,
    schedule="@weekly",
    # schedule=None,
    start_date=pendulum.datetime(2024, 1, 1, tz="UTC"),
    catchup=False,
    tags=["labs", "experiments"],
    owner_links={"gbilder": "https://www.crossref.org/people/geoffrey-bilder/"},
    description="Annotate journals route",
)
def workflow():
    @task.virtualenv(
        requirements=LIB_REQUIREMENTS,
        system_site_packages=True,
    )
    def get_title_list(run_prefix):
        from journals_route_annotation import title_list

        title_list_fn = title_list.get_title_list(run_prefix=run_prefix)

        return title_list_fn

    @task.virtualenv(
        requirements=LIB_REQUIREMENTS,
        system_site_packages=True,
    )
    def create_title_list_issn_mapping(title_list_key, run_prefix):
        from journals_route_annotation import title_list

        title_list_issn_mapping_fn = title_list.map_title_list_to_issns(
            title_list_key, run_prefix=run_prefix
        )
        return title_list_issn_mapping_fn

    @task.virtualenv(
        requirements=LIB_REQUIREMENTS,
        system_site_packages=True,
    )
    def get_issn_owner_ids(
        title_list_issn_mapping_fn,
        max_workers=10,
        debug_max=None,
        run_prefix=None,
    ):
        from journals_route_annotation import member_id

        issn_owner_ids_fn = member_id.get_issn_owner_ids(
            title_list_issn_mapping_fn,
            max_workers=max_workers,
            debug_max=debug_max,
            run_prefix=run_prefix,
        )
        return issn_owner_ids_fn

    @task.virtualenv(
        requirements=LIB_REQUIREMENTS,
        system_site_packages=True,
    )
    def create_annotations(issn_owner_ids_fn, max_workers=10):
        from journals_route_annotation import annotator

        results = annotator.parallel_add_annotations(
            issn_owner_ids_fn, max_workers=max_workers
        )

        return True

    @task.virtualenv(
        requirements=LIB_REQUIREMENTS,
        system_site_packages=True,
    )
    def generate_dag_run_prefix(dag_run_id):
        from slugify import slugify

        return slugify(dag_run_id)

    @task.virtualenv(
        requirements=LIB_REQUIREMENTS,
        system_site_packages=True,
    )
    def cleanup(_, run_prefix):
        from journals_route_annotation import crutils

        crutils.cleanup_working_path(run_prefix=run_prefix)
        return True

    # Main workflow

    debug_max = None  # Set to None if you want to run the whole thing
    max_workers = 20  # How many threads to use in tasks that support it

    run_prefix = generate_dag_run_prefix("{{ run_id }}")

    title_list_fn = get_title_list(run_prefix=run_prefix)
    title_list_issn_mapping_fn = create_title_list_issn_mapping(
        title_list_fn, run_prefix=run_prefix
    )
    issn_owner_ids_fn = get_issn_owner_ids(
        title_list_issn_mapping_fn,
        max_workers=max_workers,
        debug_max=debug_max,
        run_prefix=run_prefix,
    )

    report = create_annotations(issn_owner_ids_fn, max_workers=max_workers)
    cleanup(report, run_prefix=run_prefix)


workflow()
